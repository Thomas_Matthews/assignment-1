## DES ##
import Permutations

def permutation(block, table):
    permutated = ""
    for source_idx in table:
        permutated+=(block[source_idx])
    return permutated

def XOR(a,b):
    sum = 0
    if (a == '1'):
        sum+=1
    if (b == '1'):
        sum+=1
    return str(sum%2)

def XOR_block(a, b):
    output = ""
    for ii in range(len(a)):
        output+= XOR(a[ii], b[ii])
    return output

def s_box(block, table):
    row = binary_to_int(block[0] + block[5])
    column = binary_to_int(block[1:5])
    result = table[row][column]
    return int_to_binary(result,4)

def left_shift(key, distance):
    left_half = key[:distance]
    right_half = key[distance:]
    return right_half+left_half
    
def key_schedule_transformation(cd, shift):
    c1= left_shift(cd[:28], shift)
    d1 = left_shift(cd[28:], shift)
    cd1 = c1+d1
    return permutation(cd1, Permutations.PC2), cd1

def generate_roundkeys(bin_key):
    shifts = [1,1,2,2,2,2,2,2,1,2,2,2,2,2,2,1]
    roundkeys = []
    cd = permutation(bin_key, Permutations.PC1)
    for ii in range(16):
        roundkey, cd = key_schedule_transformation(cd, shifts[ii])
        roundkeys.append(roundkey)
    return roundkeys

def f_function(block, roundkey):
    expanded = permutation(block, Permutations.expansion)
    XORd = XOR_block(expanded, roundkey)
    s_output = ""
    for ii in range(8):
        s_input = XORd[ii*6:ii*6+6]
        s_output += s_box(s_input, Permutations.s_box[ii])
    output = permutation(s_output, Permutations.f_function)
    return output

def encryption_round(l0, r0, roundkey):
    function_output = f_function(r0, roundkey)
    l1 = r0
    r1 = XOR_block(function_output, l0)
    return l1,r1
    
def block_cryption(block, roundkeys, function):
    initial_permutation = permutation(block, Permutations.initial)
    l0 = initial_permutation[:32]
    r0 = initial_permutation[32:]
    for ii in range(16):
        if function == 'en':
            l0, r0 = encryption_round(l0,r0, roundkeys[ii])
        else:
            l0, r0 = encryption_round(l0,r0, roundkeys[15-ii])
    final_permutations = permutation(r0+l0, Permutations.final)
    return final_permutations

def encryption(input, key):
    roundkeys = generate_roundkeys(hextext_to_binary(key))
    binary_input = plaintext_to_binary(input)
    output = ""
    if (len(binary_input)%64 != 0):
        for ii in range(64 - len(binary_input)%64):
            binary_input += '0'
    for ii in range(0, len(binary_input), 64):
        block = binary_input[ii:ii+64]
        crypted_block = block_cryption(block, roundkeys, 'en')
        output += binary_to_hex(crypted_block)
    return output

def decryption(input, key):
    roundkeys = generate_roundkeys(hextext_to_binary(key))
    binary_input = hextext_to_binary(input)
    output = ""
    if (len(binary_input)%64 != 0):
        for ii in range(64 - len(binary_input)%64):
            binary_input += '0'
    for ii in range(0, len(binary_input), 64):
        block = binary_input[ii:ii+64]
        crypted_block = block_cryption(block, roundkeys, 'de')
        output += binary_to_plaintext(crypted_block)
    return output
    

## CONVERSIONS ##

def int_to_binary(int, length=0):
    binary = bin(int)[2:]
    padding = ""
    for ii in range(length-len(binary)):
        padding+='0'
    return padding+binary

def plaintext_to_binary(plaintext, length=8):
    binary = ""
    for char in plaintext:
        binary += int_to_binary(ord(char),length)
    return binary

def hextext_to_binary(hextext, length=4):
    binary = ""
    for hex in hextext:
        binary += int_to_binary(int(hex,16), length)
    return binary

def binary_to_int(bits):
    integer = 0
    for ii in range(0, len(bits)):
        selected_bit = len(bits)-1-ii
        integer += int(bits[selected_bit])*2**(ii)
    return integer

def binary_to_hex(block):
    hex_block = ""
    for ii in range(0, len(block), 4):
        nibble = block[ii:ii+4]
        hex_block += hex(int(nibble,2))[2:]
    return hex_block

def binary_to_plaintext(block):
    plaintext = ""
    for ii in range(0, len(block), 8):
        byte = block[ii:ii+8]
        plaintext += chr(binary_to_int(byte))
    return plaintext

if __name__ == "__main__":
    key = "133457799BBCDFF1"
    file = open("DES.txt")
    plain_og = file.read()
    cyphertext = encryption(plain_og, key)
    plaintext = decryption(cyphertext, key)
    print(cyphertext)
    print(plaintext)
    file.close()
