## BRUTE FORCE ##
import enchant
import string
coprimes = {1,3,5,7,9,11,15,17,19,21,23,25}
inverses = {1,9,21,15,3,19,7,23,11,5,17,25}


def decrypt_text(cyphertext, a, b):
    plaintext = ""
    for letter in cyphertext:
        plaintext += decrypt_letter(letter, a, b)
    return plaintext

def decrypt_letter(encrypted,a,b):
    decrypted = encrypted
    if isLetter(encrypted):
        decrypted = idx_to_char(((char_to_idx(encrypted)-b)*a)%26)
        if (ord(encrypted) < 97):
            decrypted = decrypted.upper()
    return decrypted

def brute_force(cypertext):
    best_key = (0,0,0) # (a, b, correct_ratio)
    for a in inverses:
        for b in range(0,26):
            plaintext = decrypt_text(cyphertext, a, b)
            correct_ratio = correctness(plaintext)
            if (correct_ratio > best_key[2]):
                best_key = (a, b, correct_ratio)
    return best_key

def correctness(decrypted):
    correct_count = 0
    word_count = 0
    dictionary = enchant.Dict("en_US")
    stripped = decrypted.translate(str.maketrans('', '', string.punctuation))
    lines = stripped.split('\n')
    for line in lines:
        words = line.split(" ")
        for word in words:
            if word != "":
                if dictionary.check(word):
                    correct_count += 1
                word_count+=1
    return correct_count/word_count

def char_to_idx(char):
    return ord(char.lower()) - 97

def idx_to_char(idx):
    return chr(idx+97)

def isLetter(char):
    lowered = char.lower()
    return ord(lowered) >= 97 and ord(lowered) <= 122

if __name__ == "__main__":
    file = open("cyphertext.txt", "r")
    cyphertext = file.read()
    best_key = brute_force(cyphertext)
    plaintext = decrypt_text(cyphertext, best_key[0], best_key[1])
    print(plaintext)
    print(best_key)
    file.close()