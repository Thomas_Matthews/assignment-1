## LETTER FREQUENCY ANALYSIS ##
import numpy as np
letterRanking = "etaoinshrdlcumwfgypbvkjxqz"

def letterCount(cyphertext):
    letterCount = np.zeros(26, dtype=int)
    for char in cyphertext:
        if isLetter(char):
            letterCount[char_to_idx(char)]+=1
    return letterCount

def sort_letters(letterCount):
    alphabet = []
    for ii in range(0, 26):
        alphabet.append(idx_to_char(ii))
    zipped = zip(letterCount, alphabet)
    sorted_list = reversed(sorted(zipped, key = lambda x: x[0]))
    return sorted_list

def create_hashmap(sorted_letters, ranking):
    hashmap = {}
    for encrypted, decrypt in zip(sorted_letters, ranking):
        hashmap[encrypted[1]] = decrypt
    return hashmap

def save_hashmap(hashmap, fileName):
    file = open(fileName, "w")
    for key, value in hashmap.items():
        mapping = key + "," + value + "\n"
        file.write(mapping)
    file.close()

def read_hashmap(fileName):
    hashmap = {}
    file = open(fileName, "r")
    text = file.readlines()
    for line in text:
        tokens = line.strip("\n").split(",")
        if (len(tokens) == 2):
            hashmap[tokens[0]] = tokens[1]
    return hashmap

def apply_hashmap_text(cyphertext, hashmap):
    plaintext = ""
    for letter in cyphertext:
        plaintext += apply_hashmap_letter(letter, hashmap)
    return plaintext

def apply_hashmap_letter(encrypted, hashmap):
    decrypted = encrypted
    if isLetter(encrypted):
        decrypted = hashmap.get(encrypted.lower())
        if (ord(encrypted) < 97):
            decrypted = decrypted.upper()
    return decrypted




## HELPERS ##
def char_to_idx(char):
    return ord(char.lower()) - 97

def idx_to_char(idx):
    return chr(idx+97)

def isLetter(char):
    lowered = char.lower()
    return ord(lowered) >= 97 and ord(lowered) <= 122

## MAIN ##
if __name__ == "__main__":
    file = open("cyphertext.txt", "r")
    cyphertext = file.read()
    letterCount = letterCount(cyphertext)
    sorted_letters = sort_letters(letterCount)
    hashmap = create_hashmap(sorted_letters, list(letterRanking))
    save_hashmap(hashmap, "auto_hashmap.txt")
    hashmap_edited = read_hashmap("edited_hashmap.txt")
    plaintext = apply_hashmap_text(cyphertext, hashmap_edited)
    print(plaintext)
    print(edited_hashmap)
    file.close()




    